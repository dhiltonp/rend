import pathlib
from unittest import mock

import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="rend")
    hub.pop.sub.add(dyne_name="exec")
    yield hub


@pytest.fixture(scope="function")
def mock_hub(hub):
    m_hub = hub.pop.testing.mock_hub()
    m_hub.OPT = mock.MagicMock()
    yield m_hub


@pytest.fixture(scope="session")
def FDIR():
    yield pathlib.Path(__file__).parent / "files"
